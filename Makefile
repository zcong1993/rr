generate:
	@go generate ./...

build: generate
	@echo "====> Build rr"
	@sh -c ./build.sh
