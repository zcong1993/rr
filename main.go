package main

import "github.com/zcong1993/utils"

func Add(a int, b int) int {
	return a + b
}

func main() {
	c := Add(1, 2)
	println(c)
	ptr := utils.StringAddress("zcong")
	println(ptr)
}
